No CSS
======

header 2
--------

### header 3
#### header 4
##### header 5
###### header 6

Regular Text
*italic text*
**Bold Text**
***Bold and Italic***
~scratch~

---

> block quote

1. Numbered
2. List
3. Yay

* bullet
* list
* yay

------------------------------------

[ ] checkbox?

```
coding
stuff
```

`inline code`
