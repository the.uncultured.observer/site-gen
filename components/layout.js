import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect } from 'react'
import styles from './layout.module.scss'
import Typography from 'typography'
import funstonTheme from 'typography-theme-funston'

const typography = new Typography(funstonTheme)
typography.injectStyles()

export default function Layout({ children, title, headerLink }) {
	return (
		<>
			<Head>
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
      <div className={styles.header}>
        <Link href={headerLink || '/'}>
          <a>{title}</a>
        </Link>
      </div>
			<div className={styles.container}>
				{children}
			</div>
		</>
	)
}
