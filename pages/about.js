import { metadata, mdToHtml } from '../lib/content'
import path from 'path'
import Layout from '../components/layout'

export async function getStaticProps() {
	const readMePath = path.join(process.cwd(), 'README.md')
	return {
		props: {
			contentHtml: await mdToHtml(readMePath),
      meta: await metadata(),
		}
	}
}

export default function About({ contentHtml, meta }) {
	return (
		<Layout title="← back to essays">
			<div dangerouslySetInnerHTML={{ __html: contentHtml }} />
		</Layout>
	)
}
